本项目原先是针对在学校期间的特定需求维护的，现在毕业了，项目没用了，已经停止维护。不接受新的PR。
尽管如此，我很高兴该项目能为广大开发者提供一定的思路。该项目将一直保留，共大家参考。

# 使用方法

1. 双击`ui.py`使用界面
2. `pdf_ltctm.py`内置了命令行工具fire库，可以用命令行调用已有命令。

> fire库用法参考[官网教程](https://python-fire.readthedocs.io/en/latest/guide.html)

所有功能均通过调用`mutool`或`pdftk server`的命令行实现，需将这两个工具放入PATH目录下或本程序目录下，程序启动时会自动将本目录临时加入环境变量。

- mutool: 在[mupdf官网](https://mupdf.com/releases/index.html)下载最新版程序，文件名如`mupdf-1.x.x-windows.zip`，解压后把`mutool.exe`放入PATH目录下或本程序目录下，另外两个exe文件可以删除。
- pdftk server
  - 在[pdftk server官网](https://www.pdflabs.com/tools/pdftk-server/)下载最新版程序，注意不要点击显眼位置的如 **Also try our friendly Windows application, PDFtk Free!** 的链接，需要往下翻一点找到下载链接。
  - 或直接下载[长期稳定版](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/pdftk_server-2.02-win-setup.exe)。
  - 该程序只有安装版，且安装后会自动添加环境变量，无需手动设置。

# 注意事项

使用合并图片功能时，图片文件夹中不能出现非图片文件，后缀名为常见的`bmp,jp(e)g,png`或其他pdf规范支持的图片格式都可以。另外，PDF文件也是合法的合并对象，但是合并后会丢失原有的目录。

使用合并功能时，若选择了自动匹配数字功能，则程序会先将匹配了数字的文件重命名为“向左侧补0至5位”，再开始合并。


# 命令行写法介绍
## mutool

[mutool官网](https://mupdf.com/docs/mutool.html)

```batch
rem ====说明：除少数特殊参数外，input文件一般在命令最后，其他选项前面都有形如-a的flag
rem 输出人眼可读目录
rem 把aa.pdf的目录输出到ee.txt
mutool show -g -o ee.txt ./aa.pdf outline

rem ====注意事项：对于那些原来就是由多张图片合成的PDF文件，应使用extract而不是convert
rem 提取图片和字体
rem 提取aa.pdf里的图片，只能放到bash当前工作目录
mutool extract ./aa.pdf

rem ====把多页PDF转换为多个PNG（无法转换为JPG）
rem 把aa.pdf转换为PNG，导出到子文件夹pics下，dpi=300
rem %05d是C语言格式化输出的表示方法，用0在数字前补齐5位，mutool会自动计数
mutool convert -o ./pics/%%05d.png -F PNG -O resolution=300(dpi，默认72) ./aa.pdf

rem ====合并多张图片转为一个PDF，可以接受PNG/JPG/BMP
mutool convert -o abc.pdf -F PDF -O compress 01.png 02.png 03.png

rem ====合并多个PDF到PDF
rem 把pdf1.pdf和pdf2.pdf合并为abc.pdf
mutool merge -o abc.pdf pdf1.pdf pdf2.pdf
rem 合并本文件夹下所有pdf，按名称排序
mutool merge -o abc.pdf *.pdf
rem 把aa.pdf和bb.pdf的指定页面合并为ty.pdf，这个命令不能用通配符
mutool merge -o ty.pdf aa.pdf 2-5 bb.pdf 3-10
```

## pdftk

[pdftk官网](https://www.pdflabs.com/tools/pdftk-server/)

```batch
rem ====读取和设置pdf附加信息和目录
rem 读取aa.pdf的目录和其他信息，导出至info.txt
rem info.txt这个文件名不能含有中文
pdftk aa.pdf dump_data_utf8 output info.txt
rem 给in.pdf附上如modified_info.txt描述的目录和其他信息，输出为out.pdf
rem pdf附上如modified_info.txt这个文件名不能含有中文
rem 新目录会完全覆盖旧目录
pdftk in.pdf update_info_utf8 modified_info.txt output out.pdf

rem ====合并多个PDF文件
rem 把aa.pdf和bb.pdf合并为yu.pdf，并拼接它们的目录
pdftk aa.pdf bb.pdf cat output yu.pdf
rem 合并本文件夹下所有pdf，按名称排序，并拼接它们的目录
pdftk *.pdf cat output qw.pdf
rem 把aa.pdf和bb.pdf的指定页面合并为yu.pdf，这个命令不能用通配符
pdftk A=aa.pdf B=bb.pdf cat A1-20 B2-5 output yu.pdf
rem 等价于python的zip_longest函数，按名称排序文件序列，然后分别放入每个文件的第1页，每个文件的第2页……，页数少的文件在填充完之后会被忽略
rem shuffle也可用cat的高级功能，如指定页面等
pdftk *.pdf shuffle output tt/out.pdf

rem ====拆分PDF
rem 把bb.pdf拆分成多个单页PDF，名称样式为page%%05d.pdf
rem 输入的PDF名称和输出的PDF组名称都可以指定绝对路径
rem 另外会在工作目录下导出bb.pdf的信息，相当于对bb.pdf使用dump_data
pdftk bb.pdf burst output page%%05d.pdf

rem ====密码相关功能
rem 注意所有跟密码相关的功能不一定总是生效，取决于对方加密用的软件
rem 解密一个有密码的文件
rem 对secured.pdf输入密码“foopass”得到unsecured.pdf这个无密码文件
pdftk secured.pdf input_pw foopass output unsecured.pdf
rem 对有具体权限设置的文件aa.pdf，赋予其全部权限（相当于解密），另存为bb.pdf
pdftk aa.pdf output bb.pdf allow AllFeatures
```

# 人工编写命令
## 用fire命令行执行已有功能

举例如下，其他功能与之类似。注意该脚本必须放在本程序目录下

```python
python pdf_ltctm.py combine_pics_in_subfolders "./" False
```

## 其他需求

本程序只整合了常见需求，有些功能的实现需要人工编写命令行。人工编写的命令行应易于人类理解和修改，现举例说明。

假设工作目录下有4000个5页的PDF，现在需要合并它们第4页到`D:/aa/bb.pdf`，python生成的命令行如下

```batch
mutool merge -o D:/aa/bb.pdf 0001.pdf 4 0002.pdf 4 ... 4000.pdf 4
```

这个命令是正确的，但是理论上不可能由人工编写。而且在Windows上此命令超过了8191字符的限制，不能直接使用。

最易于理解的写法如下

```
pdftk *.pdf shuffle output D:/aa/bb_tmp.pdf
pdftk A=D:/aa/bb_tmp.pdf cat A16001-20000 output D:/aa/bb.pdf
```

这个bat文件无需放在本程序目录下。

## 关于中文路径

若路径中含有中文，则bat文件在编写时必须保存为`utf-8`字符集，且需在bat脚本的首行写上`chcp 65001`。

另外，`pdftk server`相关命令中的部分参数不能传入含有中文的文件名，且官方文档未说明哪些功能的哪些参数不能含有中文名，只能自己尝试。目前已知要导入的目录文件名和要导出的目录的文件名不能是中文。
