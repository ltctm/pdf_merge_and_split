import PySimpleGUI as sg
from pdf_ltctm import *

# ========layout区========
PDF转图片fl = [
    [
        sg.Text("功能"),
        sg.Radio("PDF转图片", "C", True, key="PDF转图片0"),
        sg.Radio("提取图片", "C", key="PDF转图片1"),
    ],
    [
        sg.Input(key="PDF转图片-打开PDF"),
        sg.FileBrowse("打开PDF", target="PDF转图片-打开PDF", file_types=(("PDF", "*.PDF"),)),
    ],
    [
        sg.Input(key="PDF转图片-输出到"),
        sg.FolderBrowse("输出到", target="PDF转图片-输出到"),
    ],
    [sg.Text("转换图片DPI"), sg.Slider((36, 360), 300, 12, orientation="h", key="DPI")],
    [sg.Button("转换/提取", key="PDF转图片")],
]
合并fl = [
    [
        sg.Text("功能"),
        sg.Radio("合并图片", "B", True, key="合并文件类型0"),
        sg.Radio("合并PDF", "B", key="合并文件类型1"),
    ],
    [
        sg.Input(key="合并-打开文件夹"),
        sg.FolderBrowse("打开文件夹", target="合并-打开文件夹"),
    ],
    [
        sg.Input(key="合并-输出到"),
        sg.FileSaveAs("输出到", target="合并-输出到", file_types=(("PDF", "*.PDF"),)),
    ],
    [
        sg.Text("文件的排序方式"),
        sg.Radio("按文件名顺序", "A", True, key="合并排序0"),
        sg.Radio("自动匹配数字", "A", key="合并排序1"),
    ],
    [sg.Button("合并", key="合并")],
]
合并子文件夹fl = [
    [
        sg.Input(key="合并子文件夹-打开文件夹"),
        sg.FolderBrowse("打开文件夹", target="合并子文件夹-打开文件夹"),
    ],
    [
        sg.Text("文件的排序方式"),
        sg.Radio("按文件名顺序", "D", True, key="合并子文件夹排序0"),
        sg.Radio("自动匹配数字", "D", key="合并子文件夹排序1"),
    ],
    [sg.Button("合并", key="合并子文件夹")],
]
目录fl = [
    [
        sg.Input(key="目录-打开PDF"),
        sg.FileBrowse("打开PDF", target="目录-打开PDF", file_types=(("PDF", "*.PDF"),)),
    ],
    [
        sg.Input(key="目录-目录文件"),
        sg.FileBrowse("打开", target="目录-目录文件", file_types=(("目录文件", "*.json *.txt"),)),
        sg.FileSaveAs("输出到", target="目录-目录文件", file_types=(("目录文件", "*.json"),)),
    ],
    [sg.Button("导入目录", key="导入目录"), sg.Button("导出目录", key="导出目录")],
]
# ========主界面========
layout = [
    [
        sg.Column([[sg.Frame("合并", 合并fl)], [sg.Frame("合并子文件夹中的图片", 合并子文件夹fl)]]),
        sg.Column([[sg.Frame("PDF转图片/提取图片", PDF转图片fl)], [sg.Frame("目录", 目录fl)]]),
    ]
]

window = sg.Window("LTCTM的PDF小工具", layout, font=20)
# ========事件处理========


def valid_path(widget, need_exist=True):
    value = widget.get()
    if not value:
        return None
    path = Path(value)
    if need_exist and not path.exists():
        return None
    else:
        return path


tasks = []
while True:
    event, values = window.read()
    window["PDF转图片"].bind("<Button-1>", "-EVENT")
    window["合并"].bind("<Button-1>", "-EVENT")
    window["合并子文件夹"].bind("<Button-1>", "-EVENT")
    window["导入目录"].bind("<Button-1>", "-EVENT")
    window["导出目录"].bind("<Button-1>", "-EVENT")
    if event == sg.WIN_CLOSED:
        print("等待所有任务结束……")
        for t in tasks:
            t.wait()
        print("所有任务已结束！")
        break
    elif event == "PDF转图片-EVENT":
        input_path = valid_path(window["PDF转图片-打开PDF"])
        output_path = valid_path(window["PDF转图片-输出到"], False)
        if input_path and output_path:
            if window["PDF转图片0"].get():
                tasks.append(page2PNG(input_path, output_path, values["DPI"]))
            else:
                tasks.append(extract_pics(input_path, output_path))
            print("已开始PDF转图片/提取图片：%s" % input_path.absolute())
    elif event == "合并-EVENT":
        input_path = valid_path(window["合并-打开文件夹"])
        output_path = valid_path(window["合并-输出到"], False)
        if input_path and output_path:
            func = combine_pics if window["合并文件类型0"].get() else combine_pdfs
            tasks.append(func(input_path, output_path, window["合并排序1"].get()))
            print("已开始合并：%s" % input_path.absolute())
    elif event == "合并子文件夹-EVENT":
        input_path = valid_path(window["合并子文件夹-打开文件夹"])
        if input_path:
            tasks.extend(
                combine_pics_in_subfolders(input_path, window["合并子文件夹排序1"].get())
            )
            print("已开始合并子文件夹：%s" % input_path.absolute())
    elif event == "导入目录-EVENT":
        input_path = valid_path(window["目录-打开PDF"])
        tocs = valid_path(window["目录-目录文件"])
        if input_path and tocs:
            set_tocs(
                input_path, tocs, input_path.with_stem(input_path.stem + "-withtocs")
            )
            print("已完成目录设置：%s" % input_path.absolute())
    elif event == "导出目录-EVENT":
        input_path = valid_path(window["目录-打开PDF"])
        tocs = valid_path(window["目录-目录文件"], False)
        if input_path:
            output_tocs(input_path, tocs)
            print("已导出目录：%s" % tocs.absolute())
